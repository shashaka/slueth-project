package org.blog.test.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.Tracer;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
@Component
public class TraceInterceptor extends HandlerInterceptorAdapter {

    @Autowired
    private Tracer tracer;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        log.info("[{}] {} spanId : [{}], traceId : [{}]", request.getMethod(), request.getRequestURI(), tracer.getCurrentSpan().getTraceId(), tracer.getCurrentSpan().getSpanId());
        return true;
    }
}
